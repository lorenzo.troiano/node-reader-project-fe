import { Injectable } from '@angular/core';
import { Observable, Subject } from "rxjs";

export const name = 'eventService';


@Injectable({
	providedIn: 'root',
})
export class EventService {
	protected eventsMap : any = {};
	protected subscriptionsMap : any = {};
	protected _eventsSubject = new Subject<any>();
	
	constructor() {
	}
	
	public sendEvent(key: string, value: any) {
		if(this.eventsMap[key]){
			this.eventsMap[key].next({ value });
		}
	}
	
	public getEvent(key: string): Observable<any> {
		if(!this.eventsMap[key]){
			this.eventsMap[key] = new Subject<any>();
		}
		return this.eventsMap[key].asObservable();
	}
	
	public addSubscriptions(componentName: string, subscription : any){
		if(!this.subscriptionsMap[componentName]){
			this.subscriptionsMap[componentName] = [];
		}
		this.subscriptionsMap[componentName].push(subscription);
	}
	
	public clearSubscriptions(componentName: string){
		const list = this.subscriptionsMap[componentName];
		if(list){
			for(const indice in list){
				const obj = list[indice];
				obj.unsubscribe();
			}
		}
	}
}

