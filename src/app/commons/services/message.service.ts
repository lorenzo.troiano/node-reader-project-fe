import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MessageBoxComponent } from '@commons/components/message-box/message-box.component';
import { EventService } from './event.service';


export const name = 'messageService';


@Injectable({
	providedIn: 'root',
})
export class MessageService {
	messages : any[] = [];
	
	constructor(eventService : EventService, private snackBar: MatSnackBar) {
		const sub : any = eventService.getEvent("messageServiceEvent").subscribe(((event: any) => {
			if(event.value){
				event.value.forEach((message : any) => {
					snackBar.openFromComponent(MessageBoxComponent, {
						data: message,
						panelClass: ['message-box', message.messageType.style]
					});
				});
			}
		}).bind(this));
		eventService.addSubscriptions(name, sub);
	}
	
}

