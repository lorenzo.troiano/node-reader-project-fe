import { Injectable } from '@angular/core';
import { EventService } from '@commons/services/event.service';
import { Constants } from '@commons/core/utility/constants';
import { SpinnerService } from '@commons/services/spinner.service';
import { Observable } from 'rxjs';

@Injectable()
export class RestService {
	
	constructor(protected eventService: EventService, protected spinnerService : SpinnerService) {}
	
	/**
	 * Metodo che esegue l'effettiva chiamata a restangular e ne processa gli esiti, ritornando una promise adatta
	 */
	protected call<T>(restangularCall: Observable<any>, serviceName: string): Observable<any> {
		this.spinnerService.addSubscription(serviceName);
		this.spinnerService.refreshSpinner();
		return Observable.create((observer: any) => {
			return restangularCall.subscribe((res: any) => {
				this.spinnerService.removeSubscription(serviceName);
				this.spinnerService.refreshSpinner();
				observer.next(res);
			},(errResponse: any) => {
				this.spinnerService.removeSubscription(serviceName);
				this.spinnerService.refreshSpinner();
				let errors : any[] = [];
				if (errResponse) {
					let extraData = typeof(errResponse.data) === 'string' ?  errResponse.data : '';
					let error = {serviceName : serviceName, messageType: Constants.errorTypes.ERROR, message: errResponse.message + ' ' + extraData};
					errors.push(error);
				}
				
				this.eventService.sendEvent("messageServiceEvent", errors);
				
				observer.error( errResponse);
			});
		})
	}

	formatErrorMessage(codiceErrore : any, codiceErroreDesc: any, messaggioErrore: any){
		let errorMessage = messaggioErrore;
		if (codiceErroreDesc) {
			errorMessage = codiceErroreDesc;
		}

		return codiceErrore + ' - ' + errorMessage;
	}

	formatErrorMessageForErrorListElement(element : any){
		return 'Codice: ' + element.code + '; Campo: ' + element.field + '; Errore: ' + element.message;
	}

}
