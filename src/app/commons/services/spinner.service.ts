import { OnDestroy } from '@angular/core';
import { Injectable } from '@angular/core';

import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { MatSpinner } from '@angular/material/progress-spinner';
import { EventService } from '@commons/services/event.service';

export const ID = 'spinner';



@Injectable({
	providedIn: 'root',
})
export class SpinnerService{
	private spinnerTopRef;
	private showSpinner!: boolean;
	private subscriptions : any = {};
	
	constructor(private eventService: EventService, private overlay: Overlay) {
		this.spinnerTopRef= this.cdkSpinnerCreate()
	}
	
	ngOnDestroy():void{
		this.eventService.clearSubscriptions(ID);
	}
	
	private cdkSpinnerCreate() {
		return this.overlay.create({
				hasBackdrop: true,
				backdropClass: 'dark-backdrop',
				positionStrategy: this.overlay.position()
						.global()
						.centerHorizontally().centerVertically()
		})
	}
	
	public addSubscription(serviceName : string){
		this.subscriptions[serviceName] = true;
	}
	
	public removeSubscription(serviceName : string){
		this.subscriptions[serviceName] = false;
	}
	
	public refreshSpinner(){
		let showSpinner = false;
		for(let index in this.subscriptions){
			showSpinner = showSpinner ||	this.subscriptions[index];
		}
		
		if(showSpinner){
			this.show();
		}else{
			this.stop();
		}
	}
	
	public show(){
		if( !this.spinnerTopRef.hasAttached()){
			var portal = new ComponentPortal(MatSpinner);
			this.spinnerTopRef.attach(portal);
		}
	}
	
	public stop(){
		this.spinnerTopRef.detach() ;
	}
	
}
