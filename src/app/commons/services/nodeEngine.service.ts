import { Injectable } from '@angular/core';
import { Observable, Subject } from "rxjs";

export const name = 'eventService';


@Injectable({
	providedIn: 'root',
})
export class NodeEngineService {


    createNodeTree(nodeList: any, parentMap : any){
        let rootNode: any = undefined;
        let nodeMap: any = {};
        let nodeLevelMap: any = {};
        let nodeTree: any[][];
        nodeList.forEach((element : any) => {
          nodeMap[element.id] = element;
          if(element.parentId){
            if(!parentMap[element.parentId]){
              parentMap[element.parentId] = [];
            }
            parentMap[element.parentId].push(element);
          }
        });
        let maxLevel = 1;
        Object.keys(parentMap).forEach((key : any) => {
    
          let parentNode = nodeMap[key];
    
          if(!parentNode.parentId){
            rootNode = parentNode;
          }
          let level = this.calculateMaxDepth(parentNode, parentMap);
          this.addNodeToLevel(level-1, parentNode, nodeLevelMap);
          if(level>maxLevel) maxLevel = level;
        });
    
        nodeList.forEach((element : any) => {
    
          let children = parentMap[element.id];
          if(!children){
            this.addNodeToLevel(0, element, nodeLevelMap);
          }
        });
    
        nodeTree = new Array(maxLevel);
        for(let i = 0; i<maxLevel; i++){
          nodeTree[i] = nodeLevelMap[i];
        }
        nodeTree.reverse();
    
        return nodeTree;
      }
      calculateMaxDepth(parent : any, parentMap: any){
        
        if(parent == null) return 0;
        let depth=0;
        let children =  parentMap[parent.id];
        for(let elem in children){
          depth = Math.max(depth, this.calculateMaxDepth(children[elem], parentMap));
        }
        return depth+1;
      }
    
      addNodeToLevel(depth : number, node : any, nodeLevelMap: any){
        if(!nodeLevelMap[depth]){
          nodeLevelMap[depth] = [];
        }
        nodeLevelMap[depth].push(node);
      }
}