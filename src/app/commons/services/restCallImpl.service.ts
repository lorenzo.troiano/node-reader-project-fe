import { Injectable } from '@angular/core';
import { Restangular } from 'ngx-restangular';
import { Observable } from 'rxjs';
import { RestService} from '@commons/services/restCall.service'
import { EventService } from '@commons/services/event.service';
import { SpinnerService } from '@commons/services/spinner.service';
import { Constants } from '@commons/core/utility/constants';

@Injectable()
export class RestCallImpl extends RestService {
	
	constructor(protected eventService: EventService, protected spinnerService : SpinnerService, private restangular : Restangular, private constants : Constants) {
		
		super(eventService, spinnerService);
	}
	
	public callService<T>(serviceInfo: any, ...additionalParams : any[]): Observable<T> {
		if(serviceInfo.type === Constants.restCallType.GET_ALL ){
			return super.call<T>(this.restangular.all(serviceInfo.url).post(...additionalParams), serviceInfo.code);
		}
		if(serviceInfo.type === Constants.restCallType.INSERT ){
			return super.call<T>(this.restangular.all(serviceInfo.url).post(...additionalParams), serviceInfo.code);
		}
		if(serviceInfo.type === Constants.restCallType.UPDATE ){
			return super.call<T>(this.restangular.all(serviceInfo.url).customPUT(...additionalParams), serviceInfo.code);
		}
		if(serviceInfo.type === Constants.restCallType.DELETE ){
			return super.call<T>(this.restangular.one(serviceInfo.url, ...additionalParams).remove(), serviceInfo.code);
		}
		if(serviceInfo.type === Constants.restCallType.GET_ONE ){
			return super.call<T>(this.restangular.one(serviceInfo.url, ...additionalParams).get(), serviceInfo.code);
		}
		if(serviceInfo.type === Constants.restCallType.BANCOMAT_POST ){
			return super.call<T>(this.restangular.all(serviceInfo.url).post(...additionalParams), serviceInfo.code);
		}
		
		return Observable.create((observer: any) => {
			observer.next(null);
		})
	}
	
}
