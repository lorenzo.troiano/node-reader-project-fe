import { Component, Inject, OnInit } from '@angular/core';
import {MatSnackBarRef, MAT_SNACK_BAR_DATA} from '@angular/material/snack-bar';

@Component({
	selector: 'app-message-box',
	templateUrl: './message-box.component.html',
	styleUrls: ['./message-box.component.css']
})
export class MessageBoxComponent implements OnInit {
	
	constructor(public snackBarRef: MatSnackBarRef<MessageBoxComponent>,
		@Inject(MAT_SNACK_BAR_DATA) public data: any) {
	}
	
	ngOnInit(): void {
		
	}
	
}
