import { Input, SimpleChanges } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, NgModel } from '@angular/forms';
import { Constants } from '@commons/core/utility/constants';
import { AbstractValueAccessor, MakeProvider } from '../AbstractValueAccessor';
import * as uuid from 'uuid';
import { MatDialog } from '@angular/material/dialog';
import { RestCallImpl } from '@commons/services/restCallImpl.service';

@Component({
	selector: 'app-input-number',
	templateUrl: './input-number.component.html',
	styleUrls: ['./input-number.component.css', '../inputStyles.css'],
	providers: [MakeProvider(InputNumberComponent)]
})
export class InputNumberComponent extends AbstractValueAccessor implements OnInit {
	@Input() metadata!: any;
	@Input() ngModel! : NgModel;
	@Input() inputForm! : any;
	@Input() required! : boolean;
	@Input() disabled! : boolean;
	@Input() minValue! : number;
	@Input() maxValue! : number;
	@Input() customValidatorParams! : any;	
	
	public uniqueId! : string;

	constructor(private fb : FormBuilder, protected restCallImpl: RestCallImpl,  public dialog: MatDialog){
		super(undefined, Constants.inputType.INPUT_NUMBER.code, restCallImpl, dialog);
	}
	ngOnInit(): void {

	}

	ngOnChanges(changes: SimpleChanges) {
		let isSomethingChanged = false;
		let required = this.required !== undefined && this.required!==null ? this.required : undefined;
		let disabled = this.disabled !== undefined && this.disabled!==null ? this.disabled : undefined;
		let minValue = this.minValue !== undefined && this.minValue!==null ? this.minValue : undefined;
		let maxValue = this.maxValue !== undefined && this.maxValue!==null ? this.maxValue : undefined;
		let customValidatorParams = this.customValidatorParams !== undefined && this.customValidatorParams!==null ? this.customValidatorParams : undefined;
		if (changes['disabled'] && changes['disabled'].currentValue !== changes['disabled'].previousValue) {
			isSomethingChanged = true;
			disabled = changes['disabled'].currentValue;
		}
		if (changes['required'] && changes['required'].currentValue !== changes['required'].previousValue) {
			isSomethingChanged = true;
			required = changes['required'].currentValue;
		}
		if (changes['minValue'] && changes['minValue'].currentValue !== changes['minValue'].previousValue) {
			isSomethingChanged = true;
			minValue = changes['minValue'].currentValue;
		}
		if (changes['maxValue'] && changes['maxValue'].currentValue !== changes['maxValue'].previousValue) {
			isSomethingChanged = true;
			maxValue = changes['maxValue'].currentValue;
		}

		if (changes['customValidatorParams'] && changes['customValidatorParams'].currentValue !== changes['customValidatorParams'].previousValue) {
			isSomethingChanged = true;
			customValidatorParams = changes['customValidatorParams'].currentValue;
		}

		if(!this.uniqueId){
			this.uniqueId = this.metadata.name + "_" + uuid.v4();
		}

		if(isSomethingChanged){

			let configData = {
				inputForm : this.inputForm,
				name: this.uniqueId,
				required: required,
				disabled: disabled,
				minValue: minValue,
				maxValue: maxValue,
				customValidator: this.metadata.customValidator,
				customValidatorParams: customValidatorParams
			}
			super.addValidators(configData);
		}
	}
	
}
