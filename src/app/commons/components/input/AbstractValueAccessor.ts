import { forwardRef } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { Constants } from '@commons/core/utility/constants';
import { CustomValidators } from '@commons/core/validators/customValidators';
import { NumberValidator } from '@commons/core/validators/numberValidator';
import { StringValidator } from '@commons/core/validators/stringValidator';
import { MatDialog } from '@angular/material/dialog';
import { RestCallImpl } from '@commons/services/restCallImpl.service';



export abstract class AbstractValueAccessor implements ControlValueAccessor {
	
	constructor(private defaultValue : any, private inputType : string, protected restCallImpl: RestCallImpl, public dialog: MatDialog){
		
	}
	
	[x: string]: any;
	_value: any = '';
	get value(): any { return this._value; };
	set value(v: any) {
		if (v !== this._value) {
			this._value = v;
			this.onChange(v);
		}
	}
	
	writeValue(value: any) {
		if((this.defaultValue!==null && this.defaultValue!==undefined) && (value===null || value===undefined)){
			this._value = this.defaultValue;
		}else{
			this._value = value;
		}
		// warning: comment below if only want to emit on user intervention
		this.onChange(this._value);
	}
	
	onChange = (_: any) => {};
	onTouched = () => {};
	registerOnChange(fn: (_: any) => void): void { this.onChange = fn; }
	registerOnTouched(fn: () => void): void { this.onTouched = fn; }
	
	
	addValidators(configData : any){
		let inputForm = configData.inputForm;
		let name = configData.name;
		let required = configData.required;
		let disabled = configData.disabled;
		let minValue = configData.minValue;
		let maxValue = configData.maxValue;
		let minCharacters = configData.minCharacters;
		let maxCharacters = configData.maxCharacters;
		let validationPattern = configData.validationPattern;
		let customValidator = configData.customValidator;
		let customValidatorParams = configData.customValidatorParams;
				
		if(!inputForm.get(name)){
			inputForm.addControl(name, new FormControl(null));
		}
		setTimeout(() => {
			let inputFormTemp : any = inputForm.get(name);
			inputFormTemp.clearValidators();
			let validators = [];
			let asyncValidators = [];
			if(required){
				validators.push(Validators.required)
			}
			if(minCharacters!==undefined && minCharacters!==null){
				validators.push(StringValidator.minCharacters(minCharacters));
			}
			if(maxCharacters!==undefined && maxCharacters!==null){
				validators.push(StringValidator.maxCharacters(maxCharacters));
			}

			if(minValue!==undefined && minValue!==null){
				if(this.inputType === Constants.inputType.INPUT_NUMBER.code){
					validators.push(NumberValidator.min(minValue));
				}
			}
			if( maxValue!==undefined && maxValue!==null){
				if(this.inputType === Constants.inputType.INPUT_NUMBER.code){
					validators.push(NumberValidator.max(maxValue));
				}
			}
			if(validationPattern){
				validators.push(Validators.pattern(validationPattern));
			}
			if(customValidator){
				let methodName : any = customValidator;
				let customValidatorInstance: any = new CustomValidators(this.restCallImpl);
				customValidatorInstance.setParams(customValidatorParams);
				asyncValidators.push(customValidatorInstance[methodName]())
			}
			
			disabled ? inputFormTemp.disable() : inputFormTemp.enable();

			inputFormTemp.setValidators(validators);
			inputFormTemp.setAsyncValidators(asyncValidators);
			inputFormTemp.updateValueAndValidity();

		}, 0);
	}


}

export function MakeProvider(type : any){
	return {
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => type),
		multi: true
	};
}