import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { ControlContainer, FormBuilder, NgForm, NgModel } from '@angular/forms';
import { Constants } from '@commons/core/utility/constants';
import { AbstractValueAccessor, MakeProvider } from '../AbstractValueAccessor';
import * as uuid from 'uuid';
import { MatDialog } from '@angular/material/dialog';
import { RestCallImpl } from '@commons/services/restCallImpl.service';


@Component({
	selector: 'app-input-string',
	templateUrl: './input-string.component.html',
	styleUrls: ['./input-string.component.css', '../inputStyles.css'],
	providers: [MakeProvider(InputStringComponent)],
	viewProviders: [[{provide: ControlContainer, useExisting: NgForm}]]
})
export class InputStringComponent extends AbstractValueAccessor implements OnInit {
	@Input() metadata!: any;
	@Input() ngModel! : NgModel;
	@Input() inputForm! : any;
	@Input() required! : boolean;
	@Input() disabled! : boolean;
	@Input() customValidatorParams! : any;

	public uniqueId! : string;
	public errorTooltip!: string;
	
	constructor(private fb : FormBuilder, protected restCallImpl : RestCallImpl, public dialog: MatDialog){
		super(undefined, Constants.inputType.INPUT_STRING.code, restCallImpl, dialog);
	}
	
	ngOnInit(): void {
	}

	ngOnChanges(changes: SimpleChanges) {
		let isSomethingChanged = false;
		let required = this.required !== undefined && this.required!==null ? this.required : undefined;;
		let disabled = this.disabled !== undefined && this.disabled!==null ? this.disabled : undefined;
		let customValidatorParams = this.customValidatorParams !== undefined && this.customValidatorParams!==null ? this.customValidatorParams : undefined;
		if (changes['disabled'] && changes['disabled'].currentValue !== changes['disabled'].previousValue) {
			isSomethingChanged = true;
			disabled = changes['disabled'].currentValue;
		}
		if (changes['required'] && changes['required'].currentValue !== changes['required'].previousValue) {
			isSomethingChanged = true;
			required = changes['required'].currentValue;
		}

		if (changes['customValidatorParams'] && changes['customValidatorParams'].currentValue !== changes['customValidatorParams'].previousValue) {
			isSomethingChanged = true;
			customValidatorParams = changes['customValidatorParams'].currentValue;
		}
		

		if(!this.uniqueId){
			this.uniqueId = this.metadata.name + "_" + uuid.v4();
		}

		if(isSomethingChanged){

			let configData = {
				inputForm : this.inputForm,
				name: this.uniqueId,
				required: required,
				disabled: disabled,
				minCharacters: this.metadata.minCharacters,
				maxCharacters: this.metadata.maxCharacters,
				validationPattern: this.metadata.validationPattern,
				customValidator: this.metadata.customValidator,
				customValidatorParams: customValidatorParams
			}
			super.addValidators(configData);
		}
	}

}
