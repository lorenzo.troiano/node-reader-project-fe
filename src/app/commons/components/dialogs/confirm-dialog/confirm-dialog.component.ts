import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'app-confirm-dialog',
	templateUrl: './confirm-dialog.component.html',
	styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {
	title: any;
	message: any;
	
	constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: ConfirmDialogComponent) {
		// Update view with given values
		this.title = data.title ? data.title : 'COMMON.CONFIRM.CONFIRM_TITLE';
		this.message = data.message ? data.message : 'COMMON.CONFIRM.CONFIRM_MESSAGE' ;
	}
	
	ngOnInit(): void {
	}
	
	onConfirm(): void {
		// Close the dialog, return true
		this.dialogRef.close(true);
	}
	
	onDismiss(): void {
		// Close the dialog, return false
		this.dialogRef.close(false);
	}
	
}
