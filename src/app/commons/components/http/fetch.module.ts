import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { environment } from '@env/environment';
import { omitBy, toNumber } from 'lodash';



export function RestangularConfigFactory(RestangularProvider: any, http: HttpClient) {

	let baseUrl = environment.apiUrl;
	RestangularProvider.setBaseUrl(baseUrl);
	RestangularProvider.setParentless(false);
	RestangularProvider.setDefaultHttpFields({ cache: false });
	RestangularProvider.addFullRequestInterceptor(function (element: any, operation: any, path: any, url: any, headers: any, params: any) {
		//console.log('[RESTANGULAR] Intercepted request');
		params = omitBy(params, (prop) => prop === '' || prop === null || prop === undefined);
		// progressTrace.addRequest({
		//		method: operation,
		//		params,
		//		url
		// });
		const currentHeaders = headers;
		if (operation === 'get') {
			currentHeaders['Cache-Control'] = 'no-cache';
			// Fix IE11 caching
			currentHeaders['If-Modified-Since'] = '0';
		}
		return { params, headers, element };
	});
	RestangularProvider.addResponseInterceptor(function (data: any, _operation: any, _what: any, _url: any, response: any) {
		//console.log('[RESTANGULAR] Intercepted response');
		if (response.headers.get('x-total-count')) {
			data.totalCount = toNumber(response.headers.get('x-total-count'));
		}
		// progressTrace.removeRequest({
		//		method: _operation === 'getList' ? 'GET' : _operation,
		//		params: null,
		//		url: _url
		// });
		return data;
	});
	RestangularProvider.addErrorInterceptor(function (response: any, subject: any, responseHandler: any) {
		//console.log('[RESTANGULAR] Intercepted error');
		const _method = response.request.method === 0 ? 'GET' : 'POST';
		let errorText;
		if (response.status > 0 && response.statusText) {
			errorText = 'Error ' + response.status + ' - ' + response.statusText;
		} else {
			errorText = 'Generic error';
		}

		let esito = {};
		if (response.data) {
			esito = response.data;
		}
		if (response.status === 403) {
			(window as any).isExitingApp = true;
			let baseUrl = window.location.href.substr(0, window.location.href.indexOf('#'));
			if (window.location.href.indexOf('index.html#')) {
				baseUrl = window.location.href.substr(0, window.location.href.indexOf('index.html#'));
			}
			window.location.href = baseUrl + 'session-expired.html';
		} else {
			return response;
		}
		return false; // error handled
	});
}

@NgModule({
	imports: [
		CommonModule
	]
})
export class FetchModule { }
