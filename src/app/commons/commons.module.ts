import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from '../app-routing.module';
import { LayoutModule } from '@angular/cdk/layout';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material/material.module';
import { InputNumberComponent } from './components/input/input-number/input-number.component';
import { InputStringComponent } from './components/input/input-string/input-string.component';
import { InputTextareaComponent } from './components/input/input-textarea/input-textarea.component';
import { MessageBoxComponent } from './components/message-box/message-box.component';
import { ConfirmDialogComponent } from './components/dialogs/confirm-dialog/confirm-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { FetchModule } from './components/http/fetch.module';
import { CoreModule } from './core/core.module';



@NgModule({
	declarations: [
		InputStringComponent,
		InputNumberComponent,
		InputTextareaComponent,
		MessageBoxComponent,
		ConfirmDialogComponent
	],
	imports: [
		FlexLayoutModule,
		BrowserModule,
		BrowserAnimationsModule,
		AppRoutingModule,
		LayoutModule,
		MaterialModule,
		FormsModule,
		ReactiveFormsModule,
		TranslateModule,
		CoreModule
	],
	exports:[
		InputStringComponent,
		InputNumberComponent,
		InputTextareaComponent,
		FetchModule,
		CoreModule
	],
	bootstrap: []
})
export class CommonsModule { }
