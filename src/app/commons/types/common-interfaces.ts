
/////////////////////////INPUTS

export interface InputLayoutAbstract {
	name: string,
	label: string,
	tooltip?: string
}

export interface InputLayoutString extends InputLayoutAbstract {
	minCharacters?: number,
	maxCharacters: number,
	isPassword?:boolean,
	validationPattern?: string,
	customValidator?: string,
	customValidatorMessage?: string,
	errorTooltipValidator?: string,
	errorTooltipKeys?: string[]
}


export interface InputLayoutTextArea extends InputLayoutAbstract {
	maxCharacters?: number,
	maxRowsHeight?: number,
	customValidator?: string,
	customValidatorMessage?: string
	errorTooltipValidator?: string,
	errorTooltipKeys?: string[]
}

export interface InputLayoutNumber extends InputLayoutAbstract {
	decimalDigits?: number, //default 0
	thousandsSeparator?: boolean, //enabled or not, default false
	customValidator?: string,
	customValidatorMessage?: string
}