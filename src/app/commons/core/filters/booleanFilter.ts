import { Pipe, PipeTransform } from '@angular/core';
import { CommonUtilitiesService } from '@commons/core/utility/common-utilities';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';



@Pipe({ name: 'booleanFilter' })
export class BooleanFilter implements PipeTransform {
	constructor(private commonUtilitiesService: CommonUtilitiesService, private translateService: TranslateService) {
	}
	transform(value: boolean): Observable<string> {
		let codice = '';
		if (value === true) { //evito gli 0/1
			codice = 'COMMON.YES';
		}
		if (value === false) { //evito gli 0/1
			codice = 'COMMON.NO';
		}
		
		return Observable.create((observer: any) => {
			this.translateService.stream([codice]).subscribe((translation: any) => {
				observer.next(translation[codice]);
			})
		});
		
	}
	
}
