import { Pipe, PipeTransform } from '@angular/core';
import { CommonUtilitiesService } from '@commons/core/utility/common-utilities';




@Pipe({name: 'decimalFormatter'})
export class DecimalFormatter implements PipeTransform {
	
	constructor(private commonUtilitiesService: CommonUtilitiesService) {
	}
	transform(value: number | string, fractionSize: number, thousandsSeparator : boolean ) : string {
		return this.commonUtilitiesService.formattaDecimali(value, fractionSize, thousandsSeparator);
	}
	
}
