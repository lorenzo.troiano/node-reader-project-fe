import { Pipe, PipeTransform } from '@angular/core';
import { CommonUtilitiesService } from '@commons/core/utility/common-utilities';


@Pipe({name: 'leftPad'})
export class LeftPad implements PipeTransform {
	constructor(private commonUtilitiesService: CommonUtilitiesService) {
	}
	transform(value: String, paddingElement: String, paddingCardinality: number):String {
		return this.commonUtilitiesService.leftPad(value,paddingElement,paddingCardinality);
	}
}
