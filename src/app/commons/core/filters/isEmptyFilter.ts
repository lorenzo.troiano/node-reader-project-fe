import { Pipe, PipeTransform } from '@angular/core';
import { CommonUtilitiesService } from '@commons/core/utility/common-utilities';



@Pipe({name: 'isEmpty'})
export class IsEmpty implements PipeTransform {
	
	constructor(private commonUtilitiesService: CommonUtilitiesService) {
	}
	transform(obj: any):boolean {
		return this.commonUtilitiesService.isEmpty(obj);
	}
}

