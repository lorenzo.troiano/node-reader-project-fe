import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ConfirmDirective } from './directive/confirm.directive';
import { DecimalFormatterDirective } from './directive/decimalFormatter.directive';
import { BooleanFilter } from './filters/booleanFilter';
import { DecimalFormatter } from './filters/decimalFormatter';
import { IsEmpty } from './filters/isEmptyFilter';
import { LeftPad } from './filters/leftPadFilter';
import { CommonUtilitiesService } from './utility/common-utilities';

import { Constants } from './utility/constants';



@NgModule({
	imports: [CommonModule, TranslateModule, FormsModule, ReactiveFormsModule],
	exports: [
		LeftPad,
		IsEmpty,
		BooleanFilter,
		DecimalFormatter,
		DecimalFormatterDirective,
		ConfirmDirective	
	],
	declarations: [
		LeftPad,
		IsEmpty,
		BooleanFilter,
		DecimalFormatter,
		DecimalFormatterDirective,
		ConfirmDirective	
	],
	providers: [
		CommonUtilitiesService,
		Constants,
		IsEmpty,
		BooleanFilter
	]
})
export class CoreModule { }
