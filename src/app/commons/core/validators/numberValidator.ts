import { AbstractControl, FormControl, ValidatorFn } from "@angular/forms";
export class NumberValidator {
	
	constructor() { }
	
	static max(max: number): ValidatorFn {
		return (control: AbstractControl): { [key: string]: boolean } | null => {
			const val = control.value;
			
			if (val === null || val === undefined || val === "" || Number.isNaN(val)) {
				return null;
			}
			
			if (control.pristine || control.pristine) {
				return null;
			}
			if (val <= parseInt(max.toString(), 10)) {
				return null;
			}
			return { 'max': true };
		};
	}
	
	static min(min: number): ValidatorFn {
		return (control: AbstractControl): { [key: string]: boolean } | null => {
			const val = control.value;
			
			if (val === null || val === undefined || val === "" || Number.isNaN(val)) {
				return null;
			}
			
			if (control.pristine || control.pristine) {
				return null;
			}
			if (val >= parseInt(min.toString(), 10)) {
				return null;
			}
			return { 'min': true };
		};
	}
	
}
