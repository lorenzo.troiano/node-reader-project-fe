import { Injectable } from "@angular/core";
import { AbstractControl, AsyncValidatorFn, ValidationErrors } from "@angular/forms";
import { RestCallImpl } from "@commons/services/restCallImpl.service";
import { Observable,  timer } from "rxjs";
import { switchMap } from "rxjs/operators";
import { Constants } from "../utility/constants";

@Injectable({
    providedIn: 'root'
  })
export class CustomValidators {
	
    private customValidatorParams : any;

	constructor(private restCallImpl : RestCallImpl) { }

    setParams(params : any){
        this.customValidatorParams = params;
    }
    
	checkNodeExistence(): AsyncValidatorFn {
        return (control: AbstractControl): Observable<ValidationErrors | null> => {

            return timer(40)
            .pipe(
              switchMap(() : Observable<ValidationErrors | null> => {
                return Observable.create((observer: any) => {
                    if(!control.value){
                        observer.next(null);
                        observer.complete();
                    }else{
                        this.restCallImpl.callService(Constants.services.RETRIEVE_NODE, control.value).subscribe((response: any) => {
                            let evaluation =  response.data ?  null : { 'customValidator': true};
                            observer.next(evaluation);
                            observer.complete();
                        });
                    }
                })
              })
            );
        }
    }

	
}
