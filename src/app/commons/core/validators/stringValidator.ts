import { AbstractControl, FormControl, ValidatorFn } from "@angular/forms";
export class StringValidator {
	
	constructor() { }
	
	static maxCharacters(max: number): ValidatorFn {
		return (control: AbstractControl): { [key: string]: boolean } | null => {
			const val = control.value;
			if(!val || !max || val.length<=max) return null;
			return { 'maxCharacters': true };
		};
	}
	
	static minCharacters(min: number): ValidatorFn {
		return (control: AbstractControl): { [key: string]: boolean } | null => {
			const val = control.value;
			if(!val || !min || val.length>=min) return null;
			return { 'minCharacters': true };
		};
	}
	
}
