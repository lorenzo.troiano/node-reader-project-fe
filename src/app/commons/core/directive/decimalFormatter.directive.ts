import { Directive, HostListener, Input, Renderer2, ElementRef } from "@angular/core";
import { NgControl } from '@angular/forms';
import { CommonUtilitiesService } from '@commons/core/utility/common-utilities';

@Directive({ selector: "[decimalFormatter]" })
export class DecimalFormatterDirective {
	@Input() inputForm!: any;
	@Input() decimalDigits!: number;
	@Input() thousandsSeparator!: boolean;
	private controlKey = [0, 8, 13];
	private notDigit!: boolean;
	private firstInitialization: boolean = true;
	
	get ctrl() {
		return this.ngControl.control;
	}
	
	constructor(
		private ngControl: NgControl, private commonUtilitiesService: CommonUtilitiesService, private renderer: Renderer2, private element: ElementRef
	) { }
	
	@HostListener("focus", ["$event.target.value"])
	onFocus(value: any) {
		const parti = value.split(',');
		let parteIntera = parti[0];
		const parteDecimale = parti[1];
		if (parteIntera && parteIntera !== '') {
			parteIntera = this.commonUtilitiesService.rimuoviCaratteri(parteIntera, '.');
			let val = parteIntera;
			if (parteDecimale) {
				val = parteIntera + "," + parteDecimale;
			}
			const element = this.element.nativeElement;
			if (this.ctrl) {
				this.ctrl.setValue(parseFloat(val.replace(",", ".")));
				this.renderer.setProperty(element, 'value', val);
			}
		}
		
	}
	
	@HostListener("blur", ["$event.target.value"])
	onBlur(value: any) {
		if (this.ctrl) {
			const element = this.element.nativeElement;
			if(value===undefined || value === null || value ===""){
				this.ctrl.setValue(undefined);
				this.renderer.setProperty(element, 'value', "");
			}else{
				this.ctrl.setValue(parseFloat(value.replace(",", ".")));
				this.renderer.setProperty(element, 'value', this.commonUtilitiesService.formattaDecimali(value, this.decimalDigits, this.thousandsSeparator));
			}
		}
	}
	
	
	
	@HostListener('ngModelChange', ['$event'])
	ngModelChange(value: any) {
		if (this.firstInitialization) {
			const element = this.element.nativeElement;
			if(value===undefined || value === null || value === ''){
				this.renderer.setProperty(element, 'value', '');
			}else{
				this.renderer.setProperty(element, 'value', this.commonUtilitiesService.formattaDecimali(value, this.decimalDigits, this.thousandsSeparator));
			}
		}
	}
	
	@HostListener("keypress", ["$event", "$event.which", "$event.target.value", "$event.target.selectionStart", "$event.target.selectionEnd"])
	keypress(event: any, key: any, value: any, cursorPositionStart: number, cursorPositionEnd: number) {
		this.firstInitialization = false;
		let isVirgola = key === 44 && this.decimalDigits > 0;
		let numberOfCommas = (value.match(/,/g) || []).length + (isVirgola ? 1 : 0);
		let isCursorPositionAfterComma = numberOfCommas > 0 && (cursorPositionStart - cursorPositionEnd) === 0 && cursorPositionStart> value.indexOf(',');
		let numberOfDigits = this.commonUtilitiesService.countDecimals(value);
		this.notDigit = (!isVirgola && key < 47) || key === 47 || key > 57;
		if (this.notDigit || this.controlKey.indexOf(key) === 0 || numberOfCommas > 1 || (isCursorPositionAfterComma && (numberOfDigits+1)>this.decimalDigits) ) {
			return event.preventDefault();
		}
	}
	
}
