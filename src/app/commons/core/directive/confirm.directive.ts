import { Directive, HostListener, Input } from '@angular/core'
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '@commons/components/dialogs/confirm-dialog/confirm-dialog.component';

@Directive({
	selector: '[confirm]',
})
export class ConfirmDirective {
	@Input() confirmEnabled:any;
	@Input() confirmDialogTitle:any;
	@Input() confirmDialogMessage:any;
	@Input() confirmDialogCheckErrorFunction:any;	
	@Input() confirmDialogFunction:any;
	
	constructor(public dialog: MatDialog) {
		
	}
	
	@HostListener("click", ["$event"])
	onclick(value : any): void {
		let isError = this.confirmDialogCheckErrorFunction ? this.confirmDialogCheckErrorFunction() : false;
		if(isError){
			value.preventDefault();
			return;
		}
		if(this.confirmEnabled){
			value.preventDefault();
			this.dialog.open(ConfirmDialogComponent, {
				maxWidth: "400px",
				data: {title: this.confirmDialogTitle, message: this.confirmDialogMessage},
				panelClass: 'confirm-dialog'
			}).afterClosed().subscribe(dialogResult => {
				if(dialogResult){
					this.confirmDialogFunction();
				}
			});
		}else{
			this.confirmDialogFunction();
		}
	}
	
}