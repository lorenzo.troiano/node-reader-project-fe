import { Injectable } from '@angular/core';


@Injectable()
export class Constants {
	
	
	public static readonly inputType: any = {
		INPUT_STRING				: { code: 'INPUT_STRING'},
		INPUT_TEXT_AREA				: { code: 'INPUT_TEXT_AREA'},
		INPUT_NUMBER				: { code: 'INPUT_NUMBER'},
	}
	
	public static readonly restCallType: any = {
		GET_ALL: "GET_ALL",
		GET_ONE: "GET_ONE",
		INSERT: "INSERT",
		UPDATE: "UPDATE",
		DELETE: "DELETE"
	}
	
	public static readonly errorTypes: any = {
		INFO : { code: 'INFO', style : 'info'},
		WARN : { code: 'WARNING', style : 'warn'},
		ERROR : { code: 'ERROR', style : 'error'}
	}

	public static readonly services: any = {
		RETRIEVE_NODE: { code: "GET_ALL_GROUPS", type: Constants.restCallType.GET_ONE, url: "/node/retrieve" },		
		RETRIEVE_NODE_TREE: { code: "RETRIEVE_NODE_TREE", type: Constants.restCallType.GET_ALL, url: "node/retrieveNodeTree" },
		INSERT_NODE: { code: "INSERT_NODE", type: Constants.restCallType.INSERT, url: "node/insert" },
		UPDATE_NODE: { code: "UPDATE_NODE", type: Constants.restCallType.UPDATE, url: "node/update" },
		REMOVE_NODE: { code: "REMOVE_NODE", type: Constants.restCallType.DELETE, url: "node/delete" },

	};
	
	public classReference = Constants;
	
}
