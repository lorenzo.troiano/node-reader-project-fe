import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

export const name = 'commonUtilitiesService';

@Injectable()
export class CommonUtilitiesService {
	
	constructor(private route: ActivatedRoute) {
		
	}

	leftPad(value: String, paddingElement: String, paddingCardinality: number) {
		if (paddingCardinality === 0) { return value; }
		let prefix = '';
		for (let i = 0; i < paddingCardinality; i++) {
			prefix = prefix + paddingElement;
		}
		return (prefix + value).substring(value.length);
	}
	
	
	
	formattaDecimali(value: number | string, fractionSize: number, thousandsSeparator: boolean) {
		if (value === undefined || value === null || value === "") {
			return "";
		}
		const DECIMAL_SEPARATOR = ",";
		const DECIMAL_SEPARATOR_US = ".";
		const THOUSANDS_SEPARATOR = ".";
		const MILLIONS_SEPARATOR = ".";
		let PADDING = "";
		for (let i = 0; i < fractionSize; i++) {
			PADDING = PADDING + '0';
		}
		
		let integer = "0";
		let fraction = "0";
		let decimalSeparator = DECIMAL_SEPARATOR;
		
		if (value.toString().indexOf(DECIMAL_SEPARATOR_US) !== -1) {
			decimalSeparator = DECIMAL_SEPARATOR_US;
		}
		[integer, fraction = ""] = value.toString().split(decimalSeparator);
		fraction = fractionSize > 0 ? DECIMAL_SEPARATOR + (fraction + PADDING).substring(0, fractionSize) : "";
		if (thousandsSeparator) {
			integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, THOUSANDS_SEPARATOR);
		}
		return integer + fraction;
	}
	
	round(value: any, precision: any) {
		if (value === undefined) {
			return value;
		}
		if (precision === undefined) {
			precision = 0;
		}
		let negative = false;
		if (value < 0) {
			negative = true;
			value = Math.abs(value);
		}
		let k = Math.pow(10, precision);
		let nRound = (Math.round(value * k) / k).toFixed(precision);
		if (negative) {
			nRound = '-' + nRound;
		}
		return +nRound;
	}
	
	countDecimals (value : any) {
		var char_array = value.toString().split(""); // split every single char
		var not_decimal = char_array.lastIndexOf(",");
		return (not_decimal<0)?0:char_array.length - not_decimal-1;
	}
	
	rimuoviCaratteri(val: string, carDaEliminare: string): string {
		if (val.indexOf(carDaEliminare) === -1) {
			return val;
		}
		return this.rimuoviCaratteri(val.replace(carDaEliminare, ''), carDaEliminare);
	}
	
	
	stringBuild(template: string, valueMap: any): string {
		return new Function("return `" + template + "`;").call(valueMap);
	}
	
	
	markFormGroupTouched(formGroup: FormGroup) {
		(<any>Object).values(formGroup.controls).forEach((control: any) => {
			control.markAsTouched();
		});
	}

	getDottedNotation (object: any, property:any) {
		let parts = property.split('.');
		for (var i = 0; i < parts.length; i++) {
			if (object[parts[i]] !== undefined) {
				object = object[parts[i]];
			} else {
				return undefined;
			}
		}
		return object;
	}

	isEmpty(obj: any):boolean {
		if(obj === undefined || obj === null || obj === "" || obj === []) {
			return true;
		}
		
		if(parseFloat(obj) == obj && !Number.isNaN(obj)){
			return obj===undefined || obj===null;
		}
		if(typeof obj === 'function'){
			return false;
		}
		if(typeof(obj) === "boolean"){
			return obj===undefined || obj===null;
		}
		if(obj instanceof Date){
			return !obj;
		}
		
		for (const bar in obj) {
			if (obj.hasOwnProperty(bar)) {
				return false;
			}
		}
		return true;
	}
}

