import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RestCallImpl } from '@commons/services/restCallImpl.service';
import { Constants } from '@commons/core/utility/constants';
import { InputConfig } from './config/input.config';
import { MatDialog } from '@angular/material/dialog';
import { InsertNodeDialogComponent } from '@components/dialogs/insert-node-dialog/insert-node-dialog.component';
import { UpdateNodeDialogComponent } from '@components/dialogs/update-node-dialog/update-node-dialog.component';
import { EventService } from '@commons/services/event.service';
import { MessageService } from '@commons/services/message.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {


  public inputForm!: FormGroup;
  selectedNode : any;
  singleNodeTreeView : boolean = false;

  constructor(private restCallImpl: RestCallImpl, private fb: FormBuilder, public dialog: MatDialog, private eventService : EventService, private messageService: MessageService) {

  }

  ngOnInit(): void {


  }
  selectedNodeChange(selectedNode : any){
    this.selectedNode = selectedNode;
  }

  openInsertNodeDialog(){
    let width = 250;
		const dialogRef = this.dialog.open(InsertNodeDialogComponent, {
			width: width + 'px',
			maxWidth: "90vw",
			panelClass: 'profile',
			data: {
			}
		});
  }

  openUpdateNodeDialog(){
    let width = 250;
		const dialogRef = this.dialog.open(UpdateNodeDialogComponent, {
			width: width + 'px',
			maxWidth: "90vw",
			panelClass: 'profile',
			data: this.selectedNode
		});
  }

  deleteNode(){
    this.restCallImpl.callService(Constants.services.REMOVE_NODE, this.selectedNode.id).subscribe((data : any) =>{
      this.eventService.sendEvent("reloadNodeTree", {deselectNode : true});
      this.selectedNode = null;
    });
  }

  showSingleNodeTree(){
    this.eventService.sendEvent("reloadNodeTree", {id : this.selectedNode.id});
  }

  showCompleteNodeTree(){
    this.eventService.sendEvent("reloadNodeTree", {});
  }

}
