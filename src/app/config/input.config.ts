import { Injectable } from '@angular/core';
import { InputLayoutNumber, InputLayoutString, InputLayoutTextArea} from '@commons/types/common-interfaces';

@Injectable({ providedIn: 'root' })
export class InputConfig {
	

	
	public nodeInput: { ID: InputLayoutNumber, NAME : InputLayoutString, DESCRIPTION: InputLayoutTextArea, PARENT_ID: InputLayoutNumber} =
	{
		ID: { name: 'id', label: 'COMMON.ID'},
		NAME: { name: 'name', label: 'COMMON.NAME', maxCharacters: 50 },
		DESCRIPTION: { name: 'description', label: 'COMMON.DESCRIPTION', maxCharacters: 255},
		PARENT_ID: { name: 'parentId', label: 'COMMON.PARENT_ID', customValidator: 'checkNodeExistence', customValidatorMessage: 'COMMON.FORM.INVALID_PARENT_ID' },
	}
	
}


