import { Injectable, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './commons/material/material.module';
import { TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule, HttpHandler, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { Restangular, RestangularModule } from 'ngx-restangular';
import { RestCallImpl } from '@commons/services/restCallImpl.service';
import { RestService } from '@commons/services/restCall.service';
import { LayoutModule } from '@angular/cdk/layout';
import { OverlayModule } from '@angular/cdk/overlay';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonsModule } from '@commons/commons.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { RestangularConfigFactory } from '@commons/components/http/fetch.module';
import { Constants } from '@commons/core/utility/constants';
import { InsertNodeDialogComponent } from './components/dialogs/insert-node-dialog/insert-node-dialog.component';
import { UpdateNodeDialogComponent } from './components/dialogs/update-node-dialog/update-node-dialog.component';
import { NodeTreeComponent } from './components/node-tree/node-tree.component';
import { EventService } from '@commons/services/event.service';
import { MessageService } from '@commons/services/message.service';


@Injectable()
export class XhrInterceptor implements HttpInterceptor {
	
	intercept(req: HttpRequest<any>, next: HttpHandler) {
		const xhr = req.clone({
			headers: req.headers.set('X-Requested-With', 'XMLHttpRequest')
		});
		return next.handle(xhr);
	}
}

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    InsertNodeDialogComponent,
    UpdateNodeDialogComponent,
    NodeTreeComponent
  ],
  imports: [
		MaterialModule,
		FlexLayoutModule,
		BrowserModule,
		BrowserAnimationsModule,
		AppRoutingModule,
		LayoutModule,
		OverlayModule,
		FormsModule,
		ReactiveFormsModule,
		CommonsModule,
		HttpClientModule,
		TranslateModule,
		TranslateModule.forRoot({
			loader: {
					provide: TranslateLoader,
					useFactory: HttpLoaderFactory,
					deps: [HttpClient]
			},
			defaultLanguage: 'it'
		},),
		RestangularModule.forRoot([HttpClient], RestangularConfigFactory)
	],
	providers: [Restangular, RestCallImpl, RestService, EventService, MessageService,  { provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }

