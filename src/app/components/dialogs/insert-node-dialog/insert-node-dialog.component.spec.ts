import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertNodeDialogComponent } from './insert-node-dialog.component';

describe('InsertNodeDialogComponent', () => {
  let component: InsertNodeDialogComponent;
  let fixture: ComponentFixture<InsertNodeDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsertNodeDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertNodeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
