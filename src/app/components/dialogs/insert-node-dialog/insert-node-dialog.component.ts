import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { CommonUtilitiesService } from '@commons/core/utility/common-utilities';
import { Constants } from '@commons/core/utility/constants';
import { EventService } from '@commons/services/event.service';
import { RestCallImpl } from '@commons/services/restCallImpl.service';
import { InputConfig } from 'app/config/input.config';

@Component({
  selector: 'app-insert-node-dialog',
  templateUrl: './insert-node-dialog.component.html',
  styleUrls: ['./insert-node-dialog.component.css']
})
export class InsertNodeDialogComponent implements OnInit {

  nodeData : any = {};
  public inputForm!: FormGroup;

  constructor(private restCallImpl: RestCallImpl, private fb: FormBuilder, 
             public inputConfig: InputConfig, private commonUtils : CommonUtilitiesService,
             public dialogRef: MatDialogRef<InsertNodeDialogComponent>, private eventService : EventService) { }

  ngOnInit(): void {
    
    this.inputForm = this.fb.group({});
  }

  insertNode(){

    this.commonUtils.markFormGroupTouched(this.inputForm);
		if (this.inputForm.invalid) {
			return;
		}

    let dataIn = {node: this.nodeData};
    this.restCallImpl.callService(Constants.services.INSERT_NODE, dataIn).subscribe((data : any) =>{
      this.dialogRef.close();
      this.eventService.sendEvent("reloadNodeTree", {});
    });
  }

}
