import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonUtilitiesService } from '@commons/core/utility/common-utilities';
import { Constants } from '@commons/core/utility/constants';
import { EventService } from '@commons/services/event.service';
import { RestCallImpl } from '@commons/services/restCallImpl.service';
import { InputConfig } from 'app/config/input.config';

@Component({
  selector: 'app-update-node-dialog',
  templateUrl: './update-node-dialog.component.html',
  styleUrls: ['./update-node-dialog.component.css']
})
export class UpdateNodeDialogComponent implements OnInit {

  public inputForm!: FormGroup;
  nodeData : any = {};

  constructor(private restCallImpl: RestCallImpl, private fb: FormBuilder, 
    public inputConfig: InputConfig, private commonUtils : CommonUtilitiesService,
    public dialogRef: MatDialogRef<UpdateNodeDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any, private eventService : EventService) { }

  ngOnInit(): void {
    this.inputForm = this.fb.group({});
    this.nodeData = this.data;
  }

  updateNode(){

    this.commonUtils.markFormGroupTouched(this.inputForm);
		if (this.inputForm.invalid) {
			return;
		}

    let dataIn = {node: this.nodeData};
    this.restCallImpl.callService(Constants.services.UPDATE_NODE, dataIn).subscribe((data : any) =>{
      this.dialogRef.close();
      this.eventService.sendEvent("reloadNodeTree", {});
    });
  }


}
