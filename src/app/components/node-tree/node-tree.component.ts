import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Constants } from '@commons/core/utility/constants';
import { EventService } from '@commons/services/event.service';
import { NodeEngineService } from '@commons/services/nodeEngine.service';
import { RestCallImpl } from '@commons/services/restCallImpl.service';
import 'leader-line';

export const name = "node-tree";
declare var LeaderLine: any;
@Component({
  selector: 'app-node-tree',
  templateUrl: './node-tree.component.html',
  styleUrls: ['./node-tree.component.css']
})
export class NodeTreeComponent implements OnInit {

  nodeList!: any[];
  nodeTree!: any[][];
  selectedNode : any;
  lineList: any[] = [];
  
  @Output() selectedNodeChange = new EventEmitter<any>();

  constructor(private restCallImpl: RestCallImpl, private nodeEngineService : NodeEngineService, private eventService : EventService) {
    const sub: any = this.eventService.getEvent("reloadNodeTree").subscribe(((event:any) =>{
      this.reloadData(event.value.id, event.value.deselectNode);
    }).bind(this));
    this.eventService.addSubscriptions(name, sub);
   }

  ngOnInit(): void {
    this.reloadData(undefined, true);
  }

  reloadData(id? : number, deselectNode? : boolean){
      let dataIn = {id : id};
      this.restCallImpl.callService(Constants.services.RETRIEVE_NODE_TREE, dataIn).subscribe((response : any) =>{  
        this.nodeList = response.data;
        if(this.nodeList){
          if(deselectNode){
            this.selectedNode = null;
          }
          let parentMap : any = {};
          this.nodeTree = this.nodeEngineService.createNodeTree(this.nodeList, parentMap);
          this.drawLines(parentMap);

        }
      });
  }

  drawLines(parentMap : any){
    setTimeout(() => {
      this.lineList.forEach((element : any) => {
        element.remove();
      });
      this.lineList = [];
      Object.keys(parentMap).forEach((key : any) => {

        let children = parentMap[key];
        children.forEach((element : any) => {
          let line = new LeaderLine(
            document.getElementById(key),
            document.getElementById(element.id),{
              startPlug: 'square',
              endPlug: 'square'
            }                  
          );
          this.lineList.push(line);
        });

      });
  
     
    }, 100);
  }
  

  selectNode(node : any){
    if(this.selectedNode){
      if(this.selectedNode.id === node.id){

        this.selectedNode = null;
        this.selectedNodeChange.emit(null);
      }
    }else{
      this.selectedNode = node;
      this.selectedNodeChange.emit(node);
    }
  }

}
